
// get current value from element
function getElement() {
    element = document.getElementById('value');
    return element
}

// to set value on element
function setValue(value) {
    element = getElement();
    element.innerHTML = value;
    // change style color of element
    if (value == 0) {
        element.style.color = "#ffff";
    } else if (value > 0) {
        element.style.color = "#2da329";
    } else if (value < 0) {
        element.style.color = "#b43248";
    }
}


// function to increase variable
function increase() {
    value = Number(getElement().innerHTML) + 1;
    setValue(value);
}

// function to decrease variable
function decrease() {
    element = document.getElementById('value');
    value = Number(getElement().innerHTML) - 1;
    setValue(value);
}

// function to resert variable
function reset() {
    setValue(0)
}